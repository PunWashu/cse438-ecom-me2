package com.example.cse438.studio2.model

import kotlin.collections.ArrayList

class Product(
    // Identifiers
    private var id: String,

    // Manufacturer Details
    private var manufacturer: String,
    private var model: String,
    private var brand: String,

    // Category Information
    private var cat_id: Int,
    private var category: String,

    // Dimensions and Weight
    private var weight: Double,
    private var length: Double,
    private var width: Double,
    private var height: Double,

    // Images
    private var images_total: Int,
    private var imageSem3UrlStrings: ArrayList<String>,

    // Pricing
    private var price: Double,
    private var price_currency: String,

    // Product Information
    private var name: String,
    private var description: String,
    private var color: String,
    private var features: MutableMap<String, Any>,
    private var is_new: Int,

    // Important Codes
    private var upc: Long,              // Universal Product Number
    private var ean: Long,              // European  Article Number
    private var mpn: String,            // Manufacturer Part Number
    private var gtins: ArrayList<Long>, // Global Trade Item Number

    // Key Dates
    private var created_at: Long,
    private var updated_at: Long,

    // Unimportant Data
    private var variation_secondaryids: ArrayList<String>,
    private var sem3_id: String,
    private var sitedetails: ArrayList<SiteDetail>,
    private var geo: ArrayList<String>,
    private var messages: ArrayList<String>
) {

    private var imageCloudUrlStrings: ArrayList<String> = ArrayList()

    init {
        convertSem3UrlsToCloud()
    }

    private fun convertSem3UrlsToCloud() {
        val sem3UrlPreface = "http://sem3-idn.s3-website-us-east-1.amazonaws.com/"
        val cloudUrlPreface = "https://res.cloudinary.com/du3z7sadc/image/upload/v1/Apple_Products/"

        for (index in this.imageSem3UrlStrings) {
            var urlPost = index.substring(sem3UrlPreface.length until index.length)
            urlPost = urlPost.replace(',', '_')
            imageCloudUrlStrings.add(cloudUrlPreface + urlPost)
        }
    }

    //                        //
    // ------ Getters ------- //
    //                        //

    fun getId(): String {
        return this.id
    }

    fun getManufacturer(): String {
        return this.manufacturer
    }

    fun getModel(): String {
        return this.model
    }

    fun getBrand(): String {
        return this.brand
    }

    fun getCategoryId(): Int {
        return this.cat_id
    }

    fun getCategory(): String {
        return this.category
    }

    fun getWeight(): Double {
        return this.weight
    }

    fun getLength(): Double {
        return this.length
    }

    fun getWidth(): Double {
        return this.width
    }

    fun getHeight(): Double {
        return this.height
    }

    fun getImageCount(): Int {
        return this.images_total
    }

    fun getImages(): ArrayList<String> {
        return this.imageCloudUrlStrings
    }

    fun getPrice(): Double {
        return this.price
    }

    fun getPriceCurrency(): String {
        return this.price_currency
    }

    fun getProductName(): String {
        return this.name
    }

    fun getProductDescription(): String {
        return this.description
    }

    fun getProductColor(): String {
        return this.color
    }

    fun getProductFeatures(): MutableMap<String, Any> {
        return this.features
    }

    fun getIsNew(): Boolean {
        return this.is_new > 0
    }

    fun getUniversalProductNumber(): Long {
        return this.upc
    }

    fun getEuropeanArticleNumber(): Long {
        return this.ean
    }

    fun getManufacturerPartNumber(): String {
        return this.mpn
    }

    fun getGlobalTradeItemNumbers(): ArrayList<Long> {
        return this.gtins
    }

    fun getCreatedDate(): Long {
        return this.created_at
    }

    fun getUpdatedDate(): Long {
        return this.updated_at
    }
}