package com.example.cse438.studio2.model

class Offer(
    private var id: String,
    private var currency: String,
    private var firstrecorded_at: Long,
    private var price: Double,
    private var lastrecorded_at: Long,
    private var seller: String,
    private var condition: String,
    private var availability: String,
    private var isactive: Int
) {
    //                        //
    // ------ Getters ------- //
    //                        //

    fun getActiveState(): Boolean {
        return this.isactive > 0
    }
}